# Flicker client #

Android application works as a client app for Flickr. (flickr.com)

* An app uses the Flickr API (https://www.flickr.com/services/api/) and allows user searching for photos with specific words. By default, after launch application displays receent photos.
* Applicaion shows the results of the search in an infinite scroll list where each cell contains a photo.
* When tapping on a cell the user of the app must see the full screen photo and its details.


Application deployment
----------------------

Application was designed in Android Studio and uses gradle.

To generate debug apk files execute command.

```groovy
$ gradle assembleDebug
```

**Unit and integration tests are onboard.**

To run it, execute commands.

```groovy
$ gradle testDebugUnitTest
$ gradle connectedDebugAndroidTest
```

**External libraries:**

Network engine.
```groovy
compile 'com.squareup.retrofit2:retrofit:2.0.0-beta3'
compile 'com.squareup.retrofit2:converter-gson:2.0.0-beta3'
```
[Retrofit](http://square.github.io/retrofit/)


ImageLoader
```groovy
compile 'com.squareup.picasso:picasso:2.5.2'
```
[Picasso](http://square.github.io/picasso/)

JUnit + Mockito
```groovy
testCompile 'junit:junit:4.12'
testCompile 'org.mockito:mockito-core:1.10.19'
```

Powered By Artem Garkusha.