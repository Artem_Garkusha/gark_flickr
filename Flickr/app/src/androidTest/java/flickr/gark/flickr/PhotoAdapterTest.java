package flickr.gark.flickr;

import android.content.Context;
import android.test.InstrumentationTestCase;

import junit.framework.Assert;

import java.util.Arrays;

import flickr.gark.flickr.network.Photo;

/**
 * Instrumental test covers functionality of PhotoAdapter
 */
public class PhotoAdapterTest extends InstrumentationTestCase {


    private final Photo mPhoto = new Photo();
    private Context mContext;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mContext = getInstrumentation().getTargetContext();
    }

    /**
     * When updateList triggered with 2 photos objects.
     * Then adapter is filled data and getItemCount retrieves right value = 2.
     */
    public void testFillAdapterWith2Photos() {
        PhotoAdapter adapter = new PhotoAdapter(mContext, null);
        adapter.updateList(Arrays.asList(mPhoto, mPhoto));

        Assert.assertEquals(2, adapter.getItemCount());
    }

    /**
     * When updateList triggered with 2 photos objects 2 times
     * Then adapter is filled data and getItemCount retrieves right value = 4.
     */
    public void testFillAdapterWith2Photos2Times() {
        PhotoAdapter adapter = new PhotoAdapter(mContext, null);
        adapter.updateList(Arrays.asList(mPhoto, mPhoto));
        adapter.updateList(Arrays.asList(mPhoto, mPhoto));

        Assert.assertEquals(4, adapter.getItemCount());
    }


    /**
     * When updateList triggered with 2 photos and after clearData
     * Then adapter should be emty
     */
    public void testClearData() {
        PhotoAdapter adapter = new PhotoAdapter(mContext, null);
        adapter.updateList(Arrays.asList(mPhoto, mPhoto));
        adapter.clearData();

        Assert.assertEquals(0, adapter.getItemCount());
    }
}