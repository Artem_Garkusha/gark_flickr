package flickr.gark.flickr;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import flickr.gark.flickr.network.PhotoRequest;
import flickr.gark.flickr.network.Photos;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Unit test for class PhotoRequest.
 */

@RunWith(MockitoJUnitRunner.class)
public class PhotoRequestTest {

    private static final String URL = "http://url.com";
    private static final String MIME_TYPE = "application/json";
    private final CountDownLatch mCountDownLatch = new CountDownLatch(1);

    @Mock
    private PhotoRequest.PhotoSearchCallback mPhotoSearchCallback;


    /**
     * When server delivers empty response
     * Then parsing should be failed and callback triggers onFailure method.
     */
    @Test
    public void testEmptyServerResponseCode200() throws InterruptedException {
        initRequest("", 200);
        Mockito.verify(mPhotoSearchCallback).onLoadingStarted();

        mCountDownLatch.await(1L, TimeUnit.SECONDS);
        Mockito.verify(mPhotoSearchCallback).onFailure();
        mCountDownLatch.countDown();
    }


    /**
     * When server delivers no photos response
     * Then onSuccess method triggers without photos on
     */
    @Test
    public void testSuccessfulResponse() throws InterruptedException {
        String text = "{\"photos\": {\"page\": 1,\"pages\": 149179,\"perpage\": 2,\"total\": \"298358\",\"stat\": \"ok\"}}";
        initRequest(text, 200);
        Mockito.verify(mPhotoSearchCallback).onLoadingStarted();

        mCountDownLatch.await(1, TimeUnit.SECONDS);
        Mockito.verify(mPhotoSearchCallback).onSuccess(Mockito.any(Photos.class));
        mCountDownLatch.countDown();
    }


    /**
     * When server delivers no photos response
     * Then onSuccess method triggers without photos on
     */
    @Test
    public void testUnSuccessfulResponse() throws InterruptedException {
        initRequest("Server temporary Unavailable", 503);
        Mockito.verify(mPhotoSearchCallback).onLoadingStarted();

        mCountDownLatch.await(1L, TimeUnit.SECONDS);
        Mockito.verify(mPhotoSearchCallback).onFailure();
        mCountDownLatch.countDown();
    }

    private void initRequest(String textResponse, int code) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(new MockInterceptor(textResponse, code)).build();
        PhotoRequest photoRequest = new PhotoRequest(mPhotoSearchCallback, okHttpClient);
        photoRequest.requestPhotos();
    }

    /**
     * Mocked Retrofit network operation execution and provide mocked response.
     */
    private class MockInterceptor implements Interceptor {

        private String mResponse;
        private int mCode;

        public MockInterceptor(String textResponse, int code) {
            mResponse = textResponse;
            mCode = code;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            ResponseBody body = ResponseBody.create(MediaType.parse(MIME_TYPE), mResponse);
            Request request = new Request.Builder().url(URL).build();
            return new Response.Builder()
                    .request(request)
                    .code(mCode)
                    .protocol(Protocol.HTTP_1_1)
                    .body(body)
                    .build();
        }
    }

}


