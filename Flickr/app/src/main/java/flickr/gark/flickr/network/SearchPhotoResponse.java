package flickr.gark.flickr.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchPhotoResponse {

    @SerializedName("photos")
    @Expose
    private Photos photos;
    @SerializedName("stat")
    @Expose
    private String stat;

    /**
     * @return The photos
     */
    public Photos getPhotos() {
        return photos;
    }

    /**
     * @return The stat
     */
    public String getStat() {
        return stat;
    }
}
