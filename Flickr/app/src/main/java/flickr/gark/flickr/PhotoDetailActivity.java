package flickr.gark.flickr;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import flickr.gark.flickr.network.Photo;

/**
 * Activity represents and display detail information about selected photo.
 */
public class PhotoDetailActivity extends AppCompatActivity {

    private static final String PHOTO_DETAIL_TAG = "PHOTO_DETAIL_TAG";

    /**
     * Method starts activity  {@code PhotoDetailActivity} with data bundle
     *
     * @param photo Photo object to display
     */
    public static void openDetailsActivity(final Photo photo, final Context context) {
        Intent intent = new Intent(context, PhotoDetailActivity.class);
        intent.putExtra(PHOTO_DETAIL_TAG, photo);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_detail_activity);

        initToolbar();

        final Photo photo = getIntent().getParcelableExtra(PHOTO_DETAIL_TAG);
        fillUiWithData(photo);

    }

    private void initToolbar() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fillUiWithData(Photo photo) {
        ImageView imageView = (ImageView) findViewById(R.id.photo_detail);
        TextView textView = (TextView) findViewById(R.id.text_photo_detail);

        final String url = String.format(PhotoAdapter.BASE_IMAGE_URL, photo.getFarm(), photo.getServer(), photo.getId(), photo.getSecret(), "z");
        Picasso.with(this).load(url).fit().centerCrop().placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(imageView);

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(photo.getTitle()).append("\n").append(photo.getOwner());
        textView.setText(stringBuffer);
    }
}
