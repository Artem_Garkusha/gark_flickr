package flickr.gark.flickr;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import flickr.gark.flickr.network.Photo;

/**
 * Photos List Adapter encapsulates all logic for displaying photos data set.
 */
public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ItemViewHolder> implements View.OnClickListener {

    private final ArrayList<Photo> mPhotos = new ArrayList<>();
    private Picasso mPicasso;
    public static final String BASE_IMAGE_URL = "https://farm%s.staticflickr.com/%s/%s_%s_%s.jpg";
    private static final String MEDIUM = "z";
    private final OnItemSelectListener mOnItemSelectListener;


    /**
     * Callback notifies MainActivity about item click event
     */
    public interface OnItemSelectListener {

        void onItemSelected(final Photo photo);
    }

    public PhotoAdapter(final Context context, final OnItemSelectListener onItemSelectListener) {
        mOnItemSelectListener = onItemSelectListener;
        mPicasso = Picasso.with(context);
    }

    public void updateList(List<Photo> photos) {
        mPhotos.addAll(photos);
        notifyDataSetChanged();
    }

    /**
     * Erase photo list
     */
    public void clearData() {
        mPhotos.clear();
        notifyDataSetChanged();
    }


    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_list_item, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        fillContactItem(holder, position);
    }

    @Override
    public void onViewRecycled(ItemViewHolder holder) {
        super.onViewRecycled(holder);
        holder.photo.setTag(null);
        mPicasso.cancelRequest(holder.photo);
    }

    private void fillContactItem(ItemViewHolder itemViewHolder, int position) {
        Photo photo = mPhotos.get(position);

        ImageView imageView = itemViewHolder.photo;
        imageView.setTag(photo);
        imageView.setOnClickListener(this);

        final String url = String.format(BASE_IMAGE_URL, photo.getFarm(), photo.getServer(), photo.getId(), photo.getSecret(), MEDIUM);
        mPicasso.load(url).fit().centerCrop().placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(imageView);
    }

    @Override
    public int getItemCount() {
        return mPhotos.size();
    }


    @Override
    public void onClick(View v) {
        final Photo photo = (Photo) v.getTag();
        mOnItemSelectListener.onItemSelected(photo);
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        public final ImageView photo;

        public ItemViewHolder(View view) {
            super(view);
            photo = (ImageView) view;
        }
    }
}
