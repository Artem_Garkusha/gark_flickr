package flickr.gark.flickr;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import flickr.gark.flickr.network.Photo;
import flickr.gark.flickr.network.PhotoRequest;
import flickr.gark.flickr.network.Photos;

/**
 * Activity represents main application functionality with flicker photo list and infinity scrolling feature.
 * And search ability in toolbar.
 */
public class MainActivity extends AppCompatActivity implements PhotoRequest.PhotoSearchCallback, PhotoAdapter.OnItemSelectListener, SearchView.OnQueryTextListener {

    private static final String PHOTOS_INSTANCE_STATE = "PHOTOS_INSTANCE_STATE";
    private static final String QUERY_FILTER_STATE = "QUERY_FILTER_STATE";

    private PhotoAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;
    private TextView mNoResults;
    private RecyclerView.OnScrollListener mScrollListener;
    private PhotoRequest mPhotoRequest;
    private Photos mPhotos;
    private String mFilter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        initUi();
        mPhotoRequest = new PhotoRequest(this);
        handleInstanceState(savedInstanceState);
    }

    private void handleInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            mPhotoRequest.requestPhotos();
        } else {
            mPhotos = savedInstanceState.getParcelable(PHOTOS_INSTANCE_STATE);
            mFilter = savedInstanceState.getString(QUERY_FILTER_STATE);
            updateUI();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(PHOTOS_INSTANCE_STATE, mPhotos);
        outState.putString(QUERY_FILTER_STATE, mFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRecyclerView.removeOnScrollListener(mScrollListener);
    }

    private void initUi() {
        mNoResults = (TextView) findViewById(R.id.no_results);
        mRecyclerView = (RecyclerView) findViewById(R.id.photos_recycler);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mScrollListener = new RecycleScrollListener();
        mRecyclerView.addOnScrollListener(mScrollListener);

        mAdapter = new PhotoAdapter(this, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        searchView.setSubmitButtonEnabled(true);
        return true;
    }

    @Override
    public void onSuccess(Photos photos) {
        mPhotos = photos;
        updateUI();
    }


    @Override
    public void onFailure() {
        setSubtitle(R.string.loading_error);
    }

    @Override
    public void onLoadingStarted() {
        setSubtitle(R.string.loading);
    }

    private void setSubtitle(int text) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setSubtitle(getString(text));
        }
    }

    private void updateUI() {
        mAdapter.updateList(mPhotos.getPhoto());
        updateSubtitle(mPhotos);
        setNoResultVisibility();
    }

    private void setNoResultVisibility() {
        if (mPhotos != null && mPhotos.getPhoto() != null) {
            mNoResults.setVisibility(mPhotos.getPhoto().size() == 0 ? View.VISIBLE : View.INVISIBLE);
        }
    }

    private void updateSubtitle(Photos photos) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(mFilter == null ? getString(R.string.recent) : mFilter);
            String subTitle = String.format("%s/%s", photos.getPage() * photos.getPerpage(), photos.getTotal());
            actionBar.setSubtitle(subTitle);
        }
    }

    @Override
    public void onItemSelected(Photo photo) {
        PhotoDetailActivity.openDetailsActivity(photo, this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mFilter = query;
        mPhotoRequest.requestPhotos(mFilter);
        mAdapter.clearData();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    /**
     * Class responsible for calculation is recyclerViewer reached a bottom
     * also provides infinity scrolling feature.
     */
    private class RecycleScrollListener extends RecyclerView.OnScrollListener {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int childCount = mLayoutManager.getChildCount();
            int totalItemCount = mLayoutManager.getItemCount();
            int firstVisiblePosition = mLayoutManager.findFirstVisibleItemPosition();

            if ((childCount + firstVisiblePosition) >= totalItemCount) {
                mPhotoRequest.requestPhotos(mFilter, mPhotos.getPage() + 1);
            }
        }
    }
}
