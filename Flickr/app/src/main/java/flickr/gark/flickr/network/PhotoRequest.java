package flickr.gark.flickr.network;

import android.support.annotation.IntDef;
import android.support.annotation.VisibleForTesting;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Class encapsulates logic of network communication from our application and Flicker Api.
 * Based on Retrofit network engine, enqueues async network request, executes parsing and
 * notifies UI with data result.
 */
public class PhotoRequest implements Callback<SearchPhotoResponse> {

    /**
     * Callback notifies UI part with network request results.
     */
    public interface PhotoSearchCallback {
        void onSuccess(final Photos photos);

        void onFailure();

        void onLoadingStarted();
    }

    /**
     * Loading progress state.
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({IDLE, LOADING})
    public @interface LoadingState {
    }

    private static final int IDLE = 1;
    private static final int LOADING = 2;


    private static final String END_POINT = "https://api.flickr.com/services/";
    private static final String API_KEY = "b781bf44d1cf2bfde04706369308f392";
    private static final String METHOD_SEARCH = "flickr.photos.search";
    private static final String METHOD_RECENT = "flickr.photos.getRecent";
    private static final String JSON = "json";
    private static final int NO_JSON_CALLBACK = 1;
    private static final int LIMIT_PER_PAGE = 20;
    private final WeakReference<PhotoSearchCallback> mCallback;
    private final OkHttpClient mClient;
    private FlickrApi mApi;


    /**
     * Constructor which used for testing purpose.
     * Exist ability to pass mocked HttpClient.
     */
    @LoadingState
    private int mState = IDLE;

    @VisibleForTesting
    public PhotoRequest(final PhotoSearchCallback callback, final OkHttpClient client) {
        mCallback = new WeakReference<>(callback);
        mClient = client;
    }

    /**
     * Default constructor of class. Calls another constructor with default HttpClient
     */
    public PhotoRequest(final PhotoSearchCallback callback) {
        this(callback, new OkHttpClient());
    }

    /**
     * Method enqueue network call, and called after search with text submitted.
     *
     * @param filter text for search
     */
    public void requestPhotos(final String filter) {
        requestPhotos(filter, 1);
    }

    /**
     * Method enqueue network call, and called after activity initialisation.
     * So by default used page 1 and no search string request recent photos.
     */
    public void requestPhotos() {
        requestPhotos(null, 1);
    }

    /**
     * Method enqueue network call, and called from infinitive scroll feature.
     *
     * @param page requested page.
     */
    public void requestPhotos(final String filter, int page) {
        if (mState == IDLE) {
            notifyLoadingStarted();
            final String method = filter == null ? METHOD_RECENT : METHOD_SEARCH;
            Call<SearchPhotoResponse> call = getRetrofitClient().searchPhotos(JSON, method, API_KEY, NO_JSON_CALLBACK, filter, LIMIT_PER_PAGE, page);
            call.enqueue(this);
            mState = LOADING;
        }
    }

    private void notifyLoadingStarted() {
        final PhotoSearchCallback callback = mCallback.get();
        if (callback != null) {
            callback.onLoadingStarted();
        }
    }


    /**
     * Flicker api, search photos/get_recent request
     */
    public interface FlickrApi {
        @GET("rest")
        Call<SearchPhotoResponse> searchPhotos(@Query("format") String format,
                                               @Query("method") String method,
                                               @Query("api_key") String api_key,
                                               @Query("nojsoncallback") int nojsoncallback,
                                               @Query("text") String text,
                                               @Query("per_page") int perPage,
                                               @Query("page") int page);
    }

    private FlickrApi getRetrofitClient() {
        if (mApi == null) {
            mApi = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(END_POINT)
                    .client(mClient)
                    .build()
                    .create(FlickrApi.class);
        }
        return mApi;
    }


    @Override
    public void onResponse(retrofit2.Response<SearchPhotoResponse> response) {
        mState = IDLE;
        final PhotoSearchCallback callback = mCallback.get();
        if (callback != null) {
            handleResponse(response, callback);
        }
    }

    private void handleResponse(Response<SearchPhotoResponse> response, PhotoSearchCallback callback) {
        if (response.isSuccess()) {
            SearchPhotoResponse body = response.body();
            Photos photos = body.getPhotos();
            callback.onSuccess(photos);
        } else {
            callback.onFailure();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        mState = IDLE;
        final PhotoSearchCallback callback = mCallback.get();
        if (callback != null) {
            callback.onFailure();
        }
    }
}
